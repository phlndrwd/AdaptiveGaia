/*
 *   AdaptiveGaia - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Plotter.java
 *
 * This class makes use of the JFreeChart chart library in order to build a
 * stacked line graph based on the settings passed to it. It has methods to 
 * add data to the various data sets that will eventually form the basis for the
 * graph, and a method to construct the ChartPanel object that is passed back to
 * the main interface for display.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 *
 */
package plot;                  // Part of the graph package

// JFreeChart objects
import java.awt.Color;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import model.Parameters;

public class Plotter {

    private CombinedDomainXYPlot mPlot;  // Pulls top and bottom plots together
    private XYSeries mSeriesTop[];       // The data set array for the top graph
    private XYSeries mSeriesBottom;      // The data set variable for the temp.
    private XYSeriesCollection mCollectionTop;    // Collection of top data
    private XYSeriesCollection mCollectionBottom; // Collection of bottom data
    private XYDataset mDataSetTop;       // Top graph data to be plotted
    private XYDataset mDataSetBottom;    // Bottom graph data to be plotted
    private XYItemRenderer mRendererTop; // Handles top graph colours
    private XYItemRenderer mRendererBottom;  // Handles bottom graph colours
    private NumberAxis mRangeAxisTop;    // Top Y axis
    private NumberAxis mRangeAxisBottom; // Bottom Y axis
    private XYPlot mSubPlotTop;          // Top graph mPlot
    private XYPlot mSubPlotBottom;       // Bottom graph mPlot
    private ChartPanel mPanel;           // The final graph displayed
    private JFreeChart mChart;           // The graph generated

    /*
     * The constructor to initialise the graph
     * @param mutation The boolean value indicating whether the graph should
     * mPlot areas for just two or multiple mutated daisies.
     */
    public Plotter() {
        initGraph();
    }

    /*
     * Initialises the variables and sets up the graph for plotting
     */
    private void initGraph() {
        if( Parameters.isMutationEnabled() == true ) {
            mSeriesTop = new XYSeries[ 5 ];
            mSeriesTop[ 0 ] = new XYSeries( "0 - 0.2" );
            mSeriesTop[ 1 ] = new XYSeries( "0.2 - 0.4" );
            mSeriesTop[ 2 ] = new XYSeries( "0.4 - 0.6" );
            mSeriesTop[ 3 ] = new XYSeries( "0.6 - 0.8" );
            mSeriesTop[ 4 ] = new XYSeries( "0.8 - 1" );
        } else {
            mSeriesTop = new XYSeries[ 2 ];
            mSeriesTop[ 0 ] = new XYSeries( "Black" );
            mSeriesTop[ 1 ] = new XYSeries( "White" );
        }
        mSeriesBottom = new XYSeries( "Temperature" );
        NumberAxis xAxis = new NumberAxis( "Solar Luminosity" );
        xAxis.setAutoRangeIncludesZero( false );
        mPlot = new CombinedDomainXYPlot( xAxis );
        mChart = new JFreeChart( mPlot );
        mCollectionTop = new XYSeriesCollection();
        mCollectionBottom = new XYSeriesCollection();
        mRendererTop = new StandardXYItemRenderer();
        mRendererBottom = new StandardXYItemRenderer();
        mRangeAxisTop = new NumberAxis( "Grid Cover (%)" );
        mRangeAxisBottom = new NumberAxis( "Temperature (°C)" );
    }

    /*
     * Adds data to the top mPlot
     */
    public void addTopData( int index, double x, double y ) {
        mSeriesTop[ index ].add( x, y );
    }

    /*
     * Adds data to the bottom mPlot
     */
    public void addBottomData( double x, double y ) {
        mSeriesBottom.add( x, y );
    }

    /*
     * Builds and renders the graph according to the passed settings
     */
    public ChartPanel plotGraph() {
        Color colArray[];

        if( Parameters.isMutationEnabled() == true ) {
            colArray = new Color[ 5 ];
            colArray[ 0 ] = Color.black;
            colArray[ 1 ] = Color.blue;
            colArray[ 2 ] = Color.green;
            colArray[ 3 ] = Color.orange;
            colArray[ 4 ] = Color.red;
        } else {
            colArray = new Color[ 2 ];
            colArray[ 0 ] = Color.blue;
            colArray[ 1 ] = Color.red;
        }

        int colIndex = 0;

        for( int i = 0; i < mSeriesTop.length; i++ ) {
            if( mSeriesTop[ i ].getItemCount() > 0 ) {
                mCollectionTop.addSeries( mSeriesTop[ i ] );
                mRendererTop.setSeriesPaint( colIndex, colArray[ i ] );
                colIndex++;
            }
        }
        mCollectionBottom.addSeries( mSeriesBottom );
        mRendererBottom.setSeriesPaint( 0, Color.black );
        mDataSetTop = mCollectionTop;
        mDataSetBottom = mCollectionBottom;
        mSubPlotTop = new XYPlot( mDataSetTop, null, mRangeAxisTop, mRendererTop );
        mSubPlotBottom = new XYPlot( mDataSetBottom, null, mRangeAxisBottom,
                mRendererBottom );
        mSubPlotTop.setRangeAxisLocation( AxisLocation.BOTTOM_OR_LEFT );
        mSubPlotBottom.setRangeAxisLocation( AxisLocation.TOP_OR_LEFT );
        mPlot.setGap( 10.0 ); // Set the gap between each graph.
        mPlot.add( mSubPlotTop );
        mPlot.add( mSubPlotBottom );
        mPanel = new ChartPanel( mChart, true, true, true, true, false );
        return mPanel;
    }
}
