/*
 *   AdaptiveGaia - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * OptionsDialog.java
 * 
 * This class provides the interface through which the model variables can be
 * configured by the user.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package ui;                             // Part of the UI package

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.JDialog;
import model.Parameters;

public class OptionsDialog extends JDialog {

    /*
     * Creates a new instance of AGOptions, centres on screen, initialises UI
     * components, and saves the variables in the newly created variable object.
     * @param parent A reference to the parent frame
     * @param model The modal value of this dialog (set to true).
     */
    public OptionsDialog( java.awt.Frame parent, boolean modal ) {

        super( parent, modal );
        initComponents();
        saveSettings();

        // Add a window listener so that the close button acts as cancelling
        addWindowListener( new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent e ) {
                cancel();
            }
        } );
    }

    /*
     * Saves the values from the UI configuration into the variables obect.
     */
    private void saveSettings() {
        Parameters.setGridSize( Integer.parseInt( mSpinnerGridSize.getValue().toString() ) );
        Parameters.setSeedProb( Double.parseDouble( mSpinnerSeedProb.getValue().toString() ) );
        Parameters.setAlbedoGround( Double.parseDouble( mSpinnerAlbedoGround.getValue().toString() ) );
        Parameters.setInsulationParam( Double.parseDouble( mSpinnerInsulationParam.getValue().toString() ) );
        Parameters.setSolarOutput( Double.parseDouble( mSpinnerSolarOutput.getValue().toString() ) );
        Parameters.setLuminosityMax( Double.parseDouble( mSpinnerLumMax.getValue().toString() ) );
        Parameters.setLuminosityMin( Double.parseDouble( mSpinnerLumMin.getValue().toString() ) );
        Parameters.setLuminosityStep( Double.parseDouble( mSpinnerLumStep.getValue().toString() ) );
        Parameters.setAlbedoBlack( Double.parseDouble( mSpinnerAlbedoBlack.getValue().toString() ) );
        Parameters.setAlbedoWhite( Double.parseDouble( mSpinnerAlbedoWhite.getValue().toString() ) );
        Parameters.setDeathRate( Double.parseDouble( mSpinnerDeathRate.getValue().toString() ) );
        Parameters.setMutationEnabled( mCheckMutation.isSelected() );
        Parameters.setMutationRate( Double.parseDouble( mSpinnerMutRate.getValue().toString() ) );

        if( mRadioBoth.isSelected() ) {
            Parameters.setSeedMix( 0.5 );
        } else if( mRadioBlack.isSelected() ) {
            Parameters.setSeedMix( 0 );
        } else if( mRadioWhite.isSelected() ) {
            Parameters.setSeedMix( 1 );
        }
    }

    /*
     * Called when 'OK' is pressed.
     */
    private void ok() {
        saveSettings();
        setVisible( false );
    }

    /*
     * Called when 'Cancel' is pressed, restores the UI components to the saved
     * variable values.
     */
    private void cancel() {
        setVisible( false );
        mSpinnerGridSize.setValue( Parameters.getGridSize() );
        mSpinnerSeedProb.setValue( Parameters.getSeedProbability() );
        mSpinnerAlbedoGround.setValue( Parameters.getAlbedoGround() );
        mSpinnerInsulationParam.setValue( Parameters.getInsulationParam() );
        mSpinnerSolarOutput.setValue( Parameters.getSolarOutput() );
        mSpinnerLumMax.setValue( Parameters.getLuminosityMax() );
        mSpinnerLumMin.setValue( Parameters.getLuminosityMin() );
        mSpinnerLumStep.setValue( Parameters.getLuminosityStep() );
        mSpinnerAlbedoBlack.setValue( Parameters.getAlbedoBlack() );
        mSpinnerAlbedoWhite.setValue( Parameters.getAlbedoWhite() );
        mSpinnerDeathRate.setValue( Parameters.getDeathRate() );
        mCheckMutation.setSelected( Parameters.isMutationEnabled() );
        mSpinnerMutRate.setValue( Parameters.getMutationRate() );

        if( Parameters.getSeedMix() == 0.5 ) {
            mRadioBoth.setSelected( true );
        } else if( Parameters.getSeedMix() == 0 ) {
            mRadioBlack.setSelected( true );
        } else if( Parameters.getSeedMix() == 1 ) {
            mRadioWhite.setSelected( true );
        }
        resetWidgets();
    }

    /*
     * Called when 'Apply' is pressed
     */
    private void apply() {
        saveSettings();
    }

    /*
     * Ensures that the mutation rate is enabled or disabled correctly.
     */
    private void resetWidgets() {
        if( mCheckMutation.isSelected() ) {
            mSpinnerMutRate.setEnabled( true );
        } else {
            mSpinnerMutRate.setEnabled( false );
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mGroupSeed = new javax.swing.ButtonGroup();
        mPanelVariables = new javax.swing.JPanel();
        mLabelGridSize = new javax.swing.JLabel();
        mLabelProbSeed = new javax.swing.JLabel();
        SpinnerModel modelSeedProb = new SpinnerNumberModel(0.001, 0, 0.1, 0.001);
        mSpinnerSeedProb = new JSpinner(modelSeedProb);
        mLabelAlbedoGround = new javax.swing.JLabel();
        SpinnerModel modelAlbedoGround = new SpinnerNumberModel(0.5, 0, 1, 0.01);
        mSpinnerAlbedoGround = new JSpinner(modelAlbedoGround);
        mLabelInsulationParam = new javax.swing.JLabel();
        SpinnerModel modelInsulationParam = new SpinnerNumberModel(20, 0, 100, 1);
        mSpinnerInsulationParam = new JSpinner(modelInsulationParam);
        mLabelSolarOutput = new javax.swing.JLabel();
        SpinnerModel modelSolarOutput = new SpinnerNumberModel(917, 0, 10000, 1);
        mSpinnerSolarOutput = new JSpinner(modelSolarOutput);
        SpinnerModel modelGridSize = new SpinnerNumberModel(64, 2, 1000, 1);
        mSpinnerGridSize = new JSpinner(modelGridSize);
        SpinnerModel modelDeathRate = new SpinnerNumberModel(0.3, 0, 1, 0.01);
        mSpinnerDeathRate = new JSpinner(modelDeathRate);
        mLabelDeathRate = new javax.swing.JLabel();
        mCheckMutation = new javax.swing.JCheckBox();
        SpinnerModel modelMutRate = new SpinnerNumberModel(0.01, 0, 1, 0.001);
        mSpinnerMutRate = new JSpinner(modelMutRate);
        mPanelSolar = new javax.swing.JPanel();
        mLabelLumMin = new javax.swing.JLabel();
        SpinnerModel modelLumMin = new SpinnerNumberModel(0.5, 0, 0.99, 0.01);
        mSpinnerLumMin = new JSpinner(modelLumMin);
        mLabelLumMax = new javax.swing.JLabel();
        SpinnerModel modelLumMax = new SpinnerNumberModel(2, 1, 5, 0.01);
        mSpinnerLumMax = new JSpinner(modelLumMax);
        mLabelLumStep = new javax.swing.JLabel();
        SpinnerModel modelLumStep = new SpinnerNumberModel(0.004, 0.001, 0.1, 0.001);
        mSpinnerLumStep = new JSpinner(modelLumStep);
        mButtonApply = new javax.swing.JButton();
        mButtonCancel = new javax.swing.JButton();
        mButtonOK = new javax.swing.JButton();
        mPanelTwoSpecies = new javax.swing.JPanel();
        mLabelAlbedoBlack = new javax.swing.JLabel();
        SpinnerModel modelAlbedoBlack = new SpinnerNumberModel(0.25, 0, 1, 0.01);
        mSpinnerAlbedoBlack = new JSpinner(modelAlbedoBlack);
        mLabelAlbedoWhite = new javax.swing.JLabel();
        SpinnerModel modelAlbedoWhite = new SpinnerNumberModel(0.75, 0, 1, 0.01);
        mSpinnerAlbedoWhite = new JSpinner(modelAlbedoWhite);
        mRadioBlack = new javax.swing.JRadioButton();
        mRadioWhite = new javax.swing.JRadioButton();
        mRadioBoth = new javax.swing.JRadioButton();

        mGroupSeed.add(mRadioBlack);
        mGroupSeed.add(mRadioWhite);
        mGroupSeed.add(mRadioBoth);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Options");

        mPanelVariables.setBorder(javax.swing.BorderFactory.createTitledBorder("Model Variables"));

        mLabelGridSize.setText("Grid Size:");

        mLabelProbSeed.setText("Seeding Probability:");

        mLabelAlbedoGround.setText("Albedo Ground:");

        mLabelInsulationParam.setText("Insulation Parameter:");

        mLabelSolarOutput.setText("Solar Output:");

        mLabelDeathRate.setText("Death Rate:");

        mCheckMutation.setText("Mutation Rate:");
        mCheckMutation.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        mCheckMutation.setMargin(new java.awt.Insets(0, 0, 0, 0));
        mCheckMutation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mCheckMutationActionPerformed(evt);
            }
        });

        mSpinnerMutRate.setEnabled(false);

        javax.swing.GroupLayout mPanelVariablesLayout = new javax.swing.GroupLayout(mPanelVariables);
        mPanelVariables.setLayout(mPanelVariablesLayout);
        mPanelVariablesLayout.setHorizontalGroup(
            mPanelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelVariablesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mLabelSolarOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mLabelProbSeed, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mLabelAlbedoGround, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mLabelInsulationParam, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mLabelGridSize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mLabelDeathRate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mCheckMutation))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(mSpinnerMutRate)
                    .addComponent(mSpinnerDeathRate, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mSpinnerSolarOutput)
                    .addComponent(mSpinnerInsulationParam)
                    .addComponent(mSpinnerAlbedoGround)
                    .addComponent(mSpinnerSeedProb)
                    .addComponent(mSpinnerGridSize, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(142, 142, 142))
        );
        mPanelVariablesLayout.setVerticalGroup(
            mPanelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelVariablesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerGridSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelGridSize))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelProbSeed)
                    .addComponent(mSpinnerSeedProb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelAlbedoGround)
                    .addComponent(mSpinnerAlbedoGround, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelInsulationParam)
                    .addComponent(mSpinnerInsulationParam, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerSolarOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mLabelSolarOutput))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelDeathRate)
                    .addComponent(mSpinnerDeathRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mSpinnerMutRate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mCheckMutation, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        mPanelSolar.setBorder(javax.swing.BorderFactory.createTitledBorder("Solar Luminosity"));

        mLabelLumMin.setText("Min:");

        mLabelLumMax.setText("Max:");

        mLabelLumStep.setText("Step:");

        javax.swing.GroupLayout mPanelSolarLayout = new javax.swing.GroupLayout(mPanelSolar);
        mPanelSolar.setLayout(mPanelSolarLayout);
        mPanelSolarLayout.setHorizontalGroup(
            mPanelSolarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelSolarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelSolarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(mLabelLumMax, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                    .addComponent(mLabelLumMin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mLabelLumStep, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelSolarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(mSpinnerLumStep)
                    .addComponent(mSpinnerLumMax)
                    .addComponent(mSpinnerLumMin))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        mPanelSolarLayout.setVerticalGroup(
            mPanelSolarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelSolarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelSolarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelLumMin)
                    .addComponent(mSpinnerLumMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelSolarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelLumMax)
                    .addComponent(mSpinnerLumMax, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelSolarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelLumStep)
                    .addComponent(mSpinnerLumStep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        mButtonApply.setText("Apply");
        mButtonApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonApplyActionPerformed(evt);
            }
        });

        mButtonCancel.setText("Cancel");
        mButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonCancelActionPerformed(evt);
            }
        });

        mButtonOK.setText("OK");
        mButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mButtonOKActionPerformed(evt);
            }
        });

        mPanelTwoSpecies.setBorder(javax.swing.BorderFactory.createTitledBorder("Seed Species Settings"));

        mLabelAlbedoBlack.setText("Albedo Black:");

        mLabelAlbedoWhite.setText("Albedo White:");

        mRadioBlack.setText("Black");
        mRadioBlack.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        mRadioBlack.setMargin(new java.awt.Insets(0, 0, 0, 0));

        mRadioWhite.setText("White");
        mRadioWhite.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        mRadioWhite.setMargin(new java.awt.Insets(0, 0, 0, 0));

        mRadioBoth.setSelected(true);
        mRadioBoth.setText("Both");
        mRadioBoth.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        mRadioBoth.setMargin(new java.awt.Insets(0, 0, 0, 0));

        javax.swing.GroupLayout mPanelTwoSpeciesLayout = new javax.swing.GroupLayout(mPanelTwoSpecies);
        mPanelTwoSpecies.setLayout(mPanelTwoSpeciesLayout);
        mPanelTwoSpeciesLayout.setHorizontalGroup(
            mPanelTwoSpeciesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelTwoSpeciesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelTwoSpeciesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mPanelTwoSpeciesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(mLabelAlbedoBlack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(mLabelAlbedoWhite, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(mPanelTwoSpeciesLayout.createSequentialGroup()
                        .addComponent(mRadioBoth)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mRadioBlack)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelTwoSpeciesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mPanelTwoSpeciesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(mSpinnerAlbedoWhite)
                        .addComponent(mSpinnerAlbedoBlack, javax.swing.GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE))
                    .addComponent(mRadioWhite))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        mPanelTwoSpeciesLayout.setVerticalGroup(
            mPanelTwoSpeciesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mPanelTwoSpeciesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mPanelTwoSpeciesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelAlbedoBlack)
                    .addComponent(mSpinnerAlbedoBlack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mPanelTwoSpeciesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mLabelAlbedoWhite)
                    .addComponent(mSpinnerAlbedoWhite, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addGroup(mPanelTwoSpeciesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mRadioBoth)
                    .addComponent(mRadioBlack)
                    .addComponent(mRadioWhite))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mPanelVariables, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(mButtonOK)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mButtonApply))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(mPanelTwoSpecies, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mPanelSolar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mPanelVariables, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mPanelSolar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mPanelTwoSpecies, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mButtonApply)
                    .addComponent(mButtonCancel)
                    .addComponent(mButtonOK))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mCheckMutationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mCheckMutationActionPerformed
        resetWidgets();
    }//GEN-LAST:event_mCheckMutationActionPerformed

    private void mButtonApplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonApplyActionPerformed
        apply();
    }//GEN-LAST:event_mButtonApplyActionPerformed

    private void mButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonCancelActionPerformed
        cancel();
    }//GEN-LAST:event_mButtonCancelActionPerformed

    private void mButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mButtonOKActionPerformed
        ok();
    }//GEN-LAST:event_mButtonOKActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main( String args[] ) {
        java.awt.EventQueue.invokeLater( new Runnable() {
            @Override
            public void run() {
                new OptionsDialog( new javax.swing.JFrame(), true ).setVisible( true );
            }
        } );
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton mButtonApply;
    private javax.swing.JButton mButtonCancel;
    private javax.swing.JButton mButtonOK;
    private javax.swing.JCheckBox mCheckMutation;
    private javax.swing.ButtonGroup mGroupSeed;
    private javax.swing.JLabel mLabelAlbedoBlack;
    private javax.swing.JLabel mLabelAlbedoGround;
    private javax.swing.JLabel mLabelAlbedoWhite;
    private javax.swing.JLabel mLabelDeathRate;
    private javax.swing.JLabel mLabelGridSize;
    private javax.swing.JLabel mLabelInsulationParam;
    private javax.swing.JLabel mLabelLumMax;
    private javax.swing.JLabel mLabelLumMin;
    private javax.swing.JLabel mLabelLumStep;
    private javax.swing.JLabel mLabelProbSeed;
    private javax.swing.JLabel mLabelSolarOutput;
    private javax.swing.JPanel mPanelSolar;
    private javax.swing.JPanel mPanelTwoSpecies;
    private javax.swing.JPanel mPanelVariables;
    private javax.swing.JRadioButton mRadioBlack;
    private javax.swing.JRadioButton mRadioBoth;
    private javax.swing.JRadioButton mRadioWhite;
    private javax.swing.JSpinner mSpinnerAlbedoBlack;
    private javax.swing.JSpinner mSpinnerAlbedoGround;
    private javax.swing.JSpinner mSpinnerAlbedoWhite;
    private javax.swing.JSpinner mSpinnerDeathRate;
    private javax.swing.JSpinner mSpinnerGridSize;
    private javax.swing.JSpinner mSpinnerInsulationParam;
    private javax.swing.JSpinner mSpinnerLumMax;
    private javax.swing.JSpinner mSpinnerLumMin;
    private javax.swing.JSpinner mSpinnerLumStep;
    private javax.swing.JSpinner mSpinnerMutRate;
    private javax.swing.JSpinner mSpinnerSeedProb;
    private javax.swing.JSpinner mSpinnerSolarOutput;
    // End of variables declaration//GEN-END:variables
}
