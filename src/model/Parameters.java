/*
 *   AdaptiveGaia - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Parameters.java
 *
 * This class stores all of the parameters configured from the interface.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 */
package model;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;

public class Parameters {

    private static boolean mMutationEnabled = false; // A switch for albedo mutation
    private static int mGridSize = 64;               // The CA grid resolution
    private static double mSeedProbility = 0.001;    // The probability of seeding an empty site
    private static double mAlbedoGround = 0.5;       // The albedo of the bare ground
    private static double mInsulationValue = 20;     // The insulation 'q' parameter
    private static double mSolarOutput = 917;        // The solar output
    private static double mLuminosityStep = 0.004;   // The solar luminosity step value
    private static double mLuminosityMax = 2;        // The upper value for solar luminosity
    private static double mLuminosityMin = 0.5;      // The lower value for solar luminosity
    private static double mAlbedoBlack = 0.25;       // The albedo for the default black species
    private static double mAlbedoWhite = 0.75;       // The albedo for the default white species
    private static double mDeathRate = 0.3;          // The rate of death
    private static double mSeedMix = 0.5;            // Controls seeding with black, white or both
    private static double mMutationRate = 0.01;      // The rate of mMutation

    private static final double mStephanConst = 5.6704E-8; // Stephan-Boltzmann constant

    /**
     * Getter for property mMutationEnabled.
     *
     * @return Value of property mMutationEnabled.
     */
    public static boolean isMutationEnabled() {
        return mMutationEnabled;
    }

    /**
     * Setter for property mMutationEnabled.
     *
     * @param mutationEnabled New value of property mMutationEnabled.
     */
    public static void setMutationEnabled( boolean mutationEnabled ) {
        mMutationEnabled = mutationEnabled;
    }

    /**
     * Getter for property mGridSize.
     *
     * @return Value of property mGridSize.
     */
    public static int getGridSize() {
        return mGridSize;
    }

    /**
     * Setter for property mGridSize.
     *
     * @param gridSize New value of property mGridSize.
     */
    public static void setGridSize( int gridSize ) {
        mGridSize = gridSize;
    }

    /**
     * Getter for property mLuminosityMin.
     *
     * @return Value of property mLuminosityMin.
     */
    public static double getLuminosityMin() {
        return mLuminosityMin;
    }

    /**
     * Setter for property mLuminosityMin.
     *
     * @param luminosityMin New value of property mLuminosityMin.
     */
    public static void setLuminosityMin( double luminosityMin ) {
        mLuminosityMin = luminosityMin;
    }

    /**
     * Getter for property mLuminosityMax.
     *
     * @return Value of property mLuminosityMax.
     */
    public static double getLuminosityMax() {
        return mLuminosityMax;
    }

    /**
     * Setter for property mLuminosityMax.
     *
     * @param luminosityMax New value of property mLuminosityMax.
     */
    public static void setLuminosityMax( double luminosityMax ) {
        mLuminosityMax = luminosityMax;
    }

    /**
     * Getter for property mLuminosityStep.
     *
     * @return Value of property mLuminosityStep.
     */
    public static double getLuminosityStep() {
        return mLuminosityStep;
    }

    /**
     * Setter for property mLuminosityStep.
     *
     * @param luminosityStep New value of property mLuminosityStep.
     */
    public static void setLuminosityStep( double luminosityStep ) {
        mLuminosityStep = luminosityStep;
    }

    /**
     * Getter for property mSeedProbility.
     *
     * @return Value of property mSeedProbility.
     */
    public static double getSeedProbability() {
        return mSeedProbility;
    }

    /**
     * Setter for property mSeedProbility.
     *
     * @param seedProbility New value of property mSeedProbility.
     */
    public static void setSeedProb( double seedProbility ) {
        mSeedProbility = seedProbility;
    }

    /**
     * Getter for property mAlbedoBlack.
     *
     * @return Value of property mAlbedoBlack.
     */
    public static double getAlbedoBlack() {
        return mAlbedoBlack;
    }

    /**
     * Setter for property mAlbedoBlack.
     *
     * @param albedoBlack New value of property mAlbedoBlack.
     */
    public static void setAlbedoBlack( double albedoBlack ) {
        mAlbedoBlack = albedoBlack;
    }

    /**
     * Getter for property mAlbedoWhite.
     *
     * @return Value of property mAlbedoWhite.
     */
    public static double getAlbedoWhite() {
        return mAlbedoWhite;
    }

    /**
     * Setter for property mAlbedoWhite.
     *
     * @param albedoWhite New value of property mAlbedoWhite.
     */
    public static void setAlbedoWhite( double albedoWhite ) {
        mAlbedoWhite = albedoWhite;
    }

    /**
     * Getter for property mDeathRate.
     *
     * @return Value of property mDeathRate.
     */
    public static double getDeathRate() {
        return mDeathRate;
    }

    /**
     * Setter for property mDeathRate.
     *
     * @param deathRate New value of property mDeathRate.
     */
    public static void setDeathRate( double deathRate ) {
        mDeathRate = deathRate;
    }

    /**
     * Getter for property mAlbedoGround.
     *
     * @return Value of property mAlbedoGround.
     */
    public static double getAlbedoGround() {
        return mAlbedoGround;
    }

    /**
     * Setter for property mAlbedoGround.
     *
     * @param albedoGround New value of property mAlbedoGround.
     */
    public static void setAlbedoGround( double albedoGround ) {
        mAlbedoGround = albedoGround;
    }

    /**
     * Getter for property mInsulationValue.
     *
     * @return Value of property mInsulationValue.
     */
    public static double getInsulationParam() {
        return mInsulationValue;
    }

    /**
     * Setter for property mInsulationParam.
     *
     * @param insulationValue New value of property mInsulationValue.
     */
    public static void setInsulationParam( double insulationValue ) {
        mInsulationValue = insulationValue;
    }

    /**
     * Getter for property mSolarOutput.
     *
     * @return Value of property mSolarOutput.
     */
    public static double getSolarOutput() {
        return mSolarOutput;
    }

    /**
     * Setter for property mSolarOutput.
     *
     * @param solarOutput New value of property mSolarOutput.
     */
    public static void setSolarOutput( double solarOutput ) {
        mSolarOutput = solarOutput;
    }

    /**
     * Getter for property mSeedMix.
     *
     * @return Value of property mSeedMix.
     */
    public static double getSeedMix() {
        return mSeedMix;
    }

    /**
     * Setter for property mSeedMix.
     *
     * @param seedMix New value of property mSeedMix.
     */
    public static void setSeedMix( double seedMix ) {
        mSeedMix = seedMix;
    }

    /**
     * Getter for property mMutationRate.
     *
     * @return Value of property mMutationRate.
     */
    public static double getMutationRate() {
        return mMutationRate;
    }

    /**
     * Setter for property mMutationRate.
     *
     * @param mutationRate New value of property mMutationRate.
     */
    public static void setMutationRate( double mutationRate ) {
        mMutationRate = mutationRate;
    }

    /**
     * Getter for property mStephanConst.
     *
     * @return Value of property mStephanConst.
     */
    public static double getStephanConst() {
        return mStephanConst;
    }

    public static void centreWindow( Window frame ) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = ( int )( ( dimension.getWidth() - frame.getWidth() ) / 2 );
        int y = ( int )( ( dimension.getHeight() - frame.getHeight() ) / 2 );
        frame.setLocation( x, y );
    }
}
