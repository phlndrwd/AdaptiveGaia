/*
 *   AdaptiveGaia - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * DaisySite.java
 *
 * This class represents the site at each grid location, which contains the 
 * information pertaining to the local mTemperature, whether or not the site is
 * mCovered and the mDaisy species with which it might be mCovered.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 *
 */
package model;                      // Part of the model package

public class DaisySite {

    private double mTemperature;     // The local mTemperature
    private boolean mCovered;        // Whether or not the site has a mDaisy on it
    private Daisy mDaisy;

    /*
     * Creates a new instance of DaisySite and defaults to being uncovered.
     */
    public DaisySite() {
        mCovered = false;
        mDaisy = null;
    }

    /*
     * The function called to kill the mDaisy at this site.
     */
    public void killDaisy() {
        if( mCovered ) {

            mDaisy = null;
            mCovered = false;
        }
    }

    /*
     * Calculates the local mTemperature based on the global albedo, the global 
     * mTemperature, the insulation or 'q' parameter and the albedo of the ground
     * (if the site is not mCovered with a mDaisy).
     * @param albedoGlobal The global albedo
     * @param tempGlobal The global mTemperature
     * @param insulationParam The 'q' parameter to govern heat conduction
     * @param albedoGround The albedo of the bare ground
     */
    public void calculateTemp( double albedoGlobal, double tempGlobal, double insulationParam, double albedoGround ) {

        if( mCovered ) {
            mTemperature = ( insulationParam * ( albedoGlobal - mDaisy.getAlbedo() ) )
                    + tempGlobal;
        } else {
            mTemperature = ( insulationParam * ( albedoGlobal - albedoGround ) )
                    + tempGlobal;
        }
    }

    /**
     * Getter for property mTemperature.
     *
     * @return Value of property mTemperature.
     */
    public double getTemperature() {
        return mTemperature;
    }

    /**
     * Getter for property mCovered.
     *
     * @return Value of property mCovered.
     */
    public boolean isCovered() {
        return mCovered;
    }

    /**
     * Getter for property mDaisy.
     *
     * @return Value of property mDaisy.
     */
    public Daisy getDaisy() {
        return mDaisy;
    }

    /**
     * Setter for property mDaisy.
     *
     * @param daisy New value of property mDaisy.
     */
    public void setDaisy( Daisy daisy ) {
        if( !mCovered ) {
            mDaisy = daisy;
            mCovered = true;
        }
    }
}
