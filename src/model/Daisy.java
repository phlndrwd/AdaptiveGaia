/*
 *   AdaptiveGaia - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Daisy.java
 *
 * This class represents the daisies that are able to grow and inhabit the
 * fictional world on which they are seeded.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 *
 */
package model;                      // Part of the model package

public class Daisy {

    private double mAlbedo;      // The mAlbedo of the particular daisy species
    private double mGrowthRate;  // The calculated rate of growth for the daisy
    private double soloProbability;    // Stores the solo colonization probability
    private double groupProbability;   // Stores the group colonization probability
    private int mCount;          // Used to mCount the number of this species

    /*
     * Creates a new instance of Daisy
     * @param mAlbedo The set mAlbedo of this species
     */
    public Daisy( double albedo ) {
        mAlbedo = albedo;
    }

    /*
     * Calculates the colonization probability of this species occupying an
     * empty site when the set of neighbours contain other species.
     * @param probEmpty The probability of the site remaining empty
     * @param probSum The sum of the neighbour daisy solo probabilities
     */
    public void calcProbGroup( double probEmpty, double probSum ) {
        groupProbability = ( 1 - probEmpty ) * ( soloProbability / probSum );
    }

    /*
     * Calculates the colonization probability of this species occupying an
     * empty site when the set of neighbours contains only this species.
     */
    public void calcProbSolo() {
        soloProbability = 1 - Math.pow( ( 1 - mGrowthRate ), mCount );
    }

    /*
     * Increments the counter when this species has been found more than once
     * in the set of neighbouring daisies.
     */
    public void incCount() {
        mCount += 1;
    }

    /*
     * Calculates the growth of the daisy.
     * @param The local temperature of this daisy's grid site.
     */
    public double calcGrowth( double temperature ) {
        mGrowthRate = Math.max( 0, 1 - 0.003265 * Math.pow( ( 22.5 - temperature ), 2 ) );

        return mGrowthRate;
    }

    /**
     * Getter for property mAlbedo.
     *
     * @return Value of property mAlbedo.
     */
    public double getAlbedo() {
        return mAlbedo;
    }

    /**
     * Setter for property mAlbedo.
     *
     * @param albedo New value of property mAlbedo.
     */
    public void setAlbedo( double albedo ) {
        mAlbedo = albedo;
    }

    /**
     * Setter for property mCount.
     *
     * @param count New value of property mCount.
     */
    public void setCount( int count ) {
        mCount = count;
    }

    /**
     * Getter for property soloProbability.
     *
     * @return Value of property soloProbability.
     */
    public double getProbSolo() {
        return soloProbability;
    }

    /**
     * Getter for property groupProbability.
     *
     * @return Value of property groupProbability.
     */
    public double getProbGroup() {
        return groupProbability;
    }
}
