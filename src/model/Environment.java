/*
 *   AdaptiveGaia - A two-dimensional Java/NetBeans daisyworld implementation.
 *   Copyright (C) 2015 Philip J. Underwood (philjunderwood@gmail.com)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Environment.java
 *
 * This is the main model class where all data is calculated and and passed to
 * the graph object for plotting.
 *
 *  @author     Philip J. Underwood
 *  @email      philjunderwood@gmail.com
 *
 */
package model;                      // Part of the model package

import plot.Plotter;               // Used for graph plotting
import java.util.ArrayList;         // For dynamic data structures
import java.util.Iterator;          // For iterating through ArrayList objects
import java.util.Random;            // For random number generation
import org.jfree.chart.ChartPanel;  // To create and hold the plotted graph

public class Environment {

    private double mAlbedoSum;       // Sums the albedo of the whole grid
    private double mTempGlobal;      // Stores the global temperature
    private double mAlbedoGlobal;    // Stores the global albedo
    private double mAreaBlack;       // Counts daisies of 0 - 0.2 in albedo
    private double mAreaBlue;        // Counts daisies of 0.2 - 0.4 in albedo
    private double mAreaGreen;       // Counts daisies of 0.4 - 0.6 in albedo
    private double mAreaOrange;      // Counts daisies of 0.6 - 0.8 in albedo
    private double mAreaRed;         // Counts daisies of 0.8 - 1 in albedo

    private DaisySite[][] mSite;     // 2D array of grid sites in this timestep
    private DaisySite[][] mNewSite;  // 2D array of grid sites in next timestep

    private final Plotter mPlotter;  // Plots the graph
    private ChartPanel mPanel;       // Stores the graph

    // Random object used for random value generation throughout the class
    private final Random mRandom = new Random();

    /*
     * Creates a new instance of Environment, sets the variables object, initialises
     * the CA grid arrays and creates the graph object.
     */
    public Environment() {
        init();
        mPlotter = new Plotter();
    }

    /*
     * Initialises the two-dimensional arrays used to represent the surface of
     * the planet. This method is called from the class constructor.
     */
    private void init() {

        mSite = new DaisySite[ Parameters.getGridSize() ][ Parameters.getGridSize() ];
        mNewSite = new DaisySite[ Parameters.getGridSize() ][ Parameters.getGridSize() ];

        for( int i = 0; i < Parameters.getGridSize(); i++ ) {
            for( int j = 0; j < Parameters.getGridSize(); j++ ) {
                mSite[ i ][ j ] = new DaisySite();
                mNewSite[ i ][ j ] = new DaisySite();
            }
        }
    }

    /*
     * This method updates the passed list of neighbouring daisies either by
     * adding a new daisy to the list, or by incrementing the counter of a daisy
     * already in it.
     * @param i The i index of the neighbouring site to look at
     * @param j The j index of the neighbouring site to look at
     * @param daisies The list of daisies to update
     * @return The updated list of daisies
     */
    private ArrayList<Daisy> getDaisies( int i, int j, ArrayList<Daisy> daisies ) {
        Daisy daisy;
        Daisy tempDaisy;
        boolean flag = false;
        int index = 0;

        if( mSite[ i ][ j ].isCovered() ) {
            daisy = mSite[ i ][ j ].getDaisy();
            for( int k = 0; k < daisies.size(); k++ ) {
                tempDaisy = ( Daisy )daisies.get( k );
                if( daisy.getAlbedo() == tempDaisy.getAlbedo() ) {
                    flag = true;
                    index = k;
                }
            }
            if( flag ) {
                tempDaisy = ( Daisy )daisies.get( index );
                tempDaisy.incCount();
            } else {
                daisy.setCount( 1 );
                daisy.calcGrowth( mSite[ i ][ j ].getTemperature() );
                daisies.add( daisy );
            }
        }

        return daisies;
    }

    /*
     * This method takes the indexes of the subject site and surveys the
     * neighbouring sites and updates the created list of daisies by making use
     * of the getDaisies function (above).
     * @param i The i index of the subject site
     * @param j The j index of the subject site
     * @return The complete list of neighbouring daisies
     */
    private ArrayList getNeighbours( int i, int j ) {
        ArrayList<Daisy> daisies = new ArrayList<>();

        // Top left corner
        if( ( i == 0 ) && ( j == 0 ) ) {
            daisies = getDaisies( i + 1, j, daisies );
            daisies = getDaisies( i + 1, j + 1, daisies );
            daisies = getDaisies( i, j + 1, daisies );
            // Top right corner
        } else if( ( i == Parameters.getGridSize() - 1 ) && ( j == 0 ) ) {
            daisies = getDaisies( i, j + 1, daisies );
            daisies = getDaisies( i - 1, j + 1, daisies );
            daisies = getDaisies( i - 1, j, daisies );
            // Bottom left corner
        } else if( ( i == 0 ) && ( j == Parameters.getGridSize() - 1 ) ) {
            daisies = getDaisies( i, j - 1, daisies );
            daisies = getDaisies( i + 1, j - 1, daisies );
            daisies = getDaisies( i + 1, j, daisies );
            // Bottom right corner
        } else if( ( i == Parameters.getGridSize() - 1 ) && ( j == Parameters.getGridSize() - 1 ) ) {
            daisies = getDaisies( i - 1, j, daisies );
            daisies = getDaisies( i - 1, j - 1, daisies );
            daisies = getDaisies( i, j - 1, daisies );
            // Top edge
        } else if( j == 0 ) {
            daisies = getDaisies( i + 1, j, daisies );
            daisies = getDaisies( i + 1, j + 1, daisies );
            daisies = getDaisies( i, j + 1, daisies );
            daisies = getDaisies( i - 1, j + 1, daisies );
            daisies = getDaisies( i - 1, j, daisies );
            // Left edge
        } else if( i == 0 ) {
            daisies = getDaisies( i, j - 1, daisies );
            daisies = getDaisies( i + 1, j - 1, daisies );
            daisies = getDaisies( i + 1, j, daisies );
            daisies = getDaisies( i + 1, j + 1, daisies );
            daisies = getDaisies( i, j + 1, daisies );
            // Right edge
        } else if( i == Parameters.getGridSize() - 1 ) {
            daisies = getDaisies( i, j + 1, daisies );
            daisies = getDaisies( i - 1, j + 1, daisies );
            daisies = getDaisies( i - 1, j, daisies );
            daisies = getDaisies( i - 1, j - 1, daisies );
            daisies = getDaisies( i, j - 1, daisies );
            // Bottom edge
        } else if( j == Parameters.getGridSize() - 1 ) {
            daisies = getDaisies( i - 1, j, daisies );
            daisies = getDaisies( i - 1, j - 1, daisies );
            daisies = getDaisies( i, j - 1, daisies );
            daisies = getDaisies( i + 1, j - 1, daisies );
            daisies = getDaisies( i + 1, j, daisies );
            // Anywhere else
        } else {
            daisies = getDaisies( i, j - 1, daisies );
            daisies = getDaisies( i + 1, j - 1, daisies );
            daisies = getDaisies( i + 1, j, daisies );
            daisies = getDaisies( i + 1, j + 1, daisies );
            daisies = getDaisies( i, j + 1, daisies );
            daisies = getDaisies( i - 1, j + 1, daisies );
            daisies = getDaisies( i - 1, j, daisies );
            daisies = getDaisies( i - 1, j - 1, daisies );
        }

        return daisies;
    }

    /*
     * A basic probability function that takes a probability and generates a
     * boolean true in value with a probability of that passed as a parameter.
     * @param probability The passed probability
     * @return The returned boolean value
     */
    private boolean booleanWithProbability( double probability ) {

        boolean result = mRandom.nextDouble() <= probability;

        return result;
    }

    /*
     * This function receives a value for daisy albedo as a parameter then
     * increments the appropriate counter in order to calculate the grid
     * coverage of daisies within the specified ranges.
     */
    private void sumArea( double albedo ) {
        if( ( albedo >= 0 ) && ( albedo <= 0.2 ) ) {
            mAreaBlack++;
        } else if( ( albedo > 0.2 ) && ( albedo <= 0.4 ) ) {
            mAreaBlue++;
        } else if( ( albedo > 0.4 ) && ( albedo <= 0.6 ) ) {
            mAreaGreen++;
        } else if( ( albedo > 0.6 ) && ( albedo <= 0.8 ) ) {
            mAreaOrange++;
        } else if( ( albedo > 0.8 ) && ( albedo <= 1 ) ) {
            mAreaRed++;
        }
    }

    /*
     * This takes an input corresponding to the sum of sites covered by a 
     * particular daisy species, this is then divided by the grid resolution and
     * so multiplied by 100 so as to scale the variable to a percentage cover of
     * the whole CA.
     * @param input The sum area to be scaled
     * @return The scaled value
     */
    private double scaleArea( double area ) {
        double scaledArea = ( area / Math.pow( Parameters.getGridSize(), 2 ) ) * 100;

        return scaledArea;
    }

    /*
     * This function is called at the end of a complete pass of the CA grid in
     * order to reset the area counters.
     */
    private void resetVariables() {
        mAlbedoSum = 0;
        mAreaBlack = 0;
        mAreaBlue = 0;
        mAreaGreen = 0;
        mAreaOrange = 0;
        mAreaRed = 0;
    }

    /*
     * Calculates the global temperature based on the solar luminosity and the
     * global albedo.
     * @param solarLum The solar luminosity
     * @param albedoGlobal The global albedo
     */
    private void calcTempGlobal( double solarLum, double albedoGlobal ) {
        mTempGlobal = Math.pow( ( ( Parameters.getSolarOutput() * solarLum * ( 1 - albedoGlobal ) ) / Parameters.getStephanConst() ), 0.25 ) - 273;
    }

    /*
     * Given an particular array location the site at the next time step is 
     * seeded with a probability of the configured seeding probability, with a 
     * mix determined by whether the seed is to black only, white only or mixed.
     * @param i The i array index
     * @param The j array index
     */
    private void seedLoc( int i, int j ) {
        double prob = mRandom.nextDouble();

        if( booleanWithProbability( Parameters.getSeedProbability() ) ) {
            if( prob > Parameters.getSeedMix() ) {
                mNewSite[ i ][ j ].setDaisy( new Daisy( Parameters.getAlbedoBlack() ) );
            } else {
                mNewSite[ i ][ j ].setDaisy( new Daisy( Parameters.getAlbedoWhite() ) );
            }
        }
    }

    /*
     * The function takes an albedo representing a particular daisy species and
     * generates a random mutation based on a distribution of the specified 
     * mutation rate.
     * @param albedo The albedo of the daisy to mutate.
     * @return The mutated albedo.
     */
    private double mutateAlbedo( double albedo ) {
        // Pick random value within a distribution +/- of mutation rate
        double mutation = 2 * Parameters.getMutationRate() * mRandom.nextDouble() - Parameters.getMutationRate();

        // Alter albedo according to mutation
        albedo += mutation;

        // Clamp result
        if( albedo < 0 ) {
            albedo = 0;
        }
        if( albedo > 1 ) {
            albedo = 1;
        }

        return albedo;
    }

    /*
     * The main method from which all core system functionality is orchestrated.
     */
    public void run() {

        ArrayList daisies;
        Daisy daisy;

        double probSum;
        double probEmpty;
        mAlbedoGlobal = 0.5;

        // Luminosity loop
        for( double lum = Parameters.getLuminosityMin(); lum < Parameters.getLuminosityMax();
                lum += Parameters.getLuminosityStep() ) {
            calcTempGlobal( lum, mAlbedoGlobal );
            // Horizontal array loop
            for( int i = 0; i < Parameters.getGridSize(); i++ ) {
                // Vertical array loop
                for( int j = 0; j < Parameters.getGridSize(); j++ ) {
                    probEmpty = 1;
                    probSum = 0;
                    mSite[ i ][ j ].calculateTemp( mAlbedoGlobal, mTempGlobal,
                            Parameters.getInsulationParam(), Parameters.getAlbedoGround() );
                    // Is covered
                    if( mSite[ i ][ j ].isCovered() ) {
                        double albedo = mSite[ i ][ j ].getDaisy().getAlbedo();
                        mAlbedoSum += albedo;
                        sumArea( albedo );
                        if( booleanWithProbability( Parameters.getDeathRate() ) ) {
                            mNewSite[ i ][ j ].killDaisy();
                        }
                        // Isn't covered
                    } else {
                        mAlbedoSum += Parameters.getAlbedoGround();
                        daisies = getNeighbours( i, j );
                        for( Iterator it = daisies.iterator(); it.hasNext(); ) {
                            daisy = ( Daisy )it.next();
                            daisy.calcProbSolo();
                            probEmpty *= ( 1 - daisy.getProbSolo() );
                            probSum += daisy.getProbSolo();
                        }
                        if( daisies.size() > 1 ) {
                            for( Iterator it = daisies.iterator(); it.hasNext(); ) {
                                daisy = ( Daisy )it.next();
                                daisy.calcProbGroup( probEmpty, probSum );
                            }
                        }
                        Daisy parent = null;
                        Daisy tempDaisy;

                        double prob = mRandom.nextDouble();
                        if( daisies.size() == 1 ) {
                            tempDaisy = ( Daisy )daisies.get( 0 );
                            if( booleanWithProbability( tempDaisy.getProbSolo() ) ) {
                                parent = tempDaisy;
                            }
                        } else if( daisies.size() > 1 ) {
                            double tempProb = 0;
                            for( Iterator it = daisies.iterator(); it.hasNext(); ) {
                                tempDaisy = ( Daisy )it.next();
                                tempProb += tempDaisy.getProbGroup();
                                if( ( prob < tempProb ) && ( parent == null ) ) {
                                    parent = tempDaisy;
                                }
                            }
                        }
                        if( parent != null ) {
                            if( Parameters.isMutationEnabled() ) {
                                parent.setAlbedo( mutateAlbedo( parent.getAlbedo() ) );
                            }
                            mNewSite[ i ][ j ].setDaisy( parent );
                        } else {
                            seedLoc( i, j );
                        }
                    }
                }
            }
            mAlbedoGlobal = mAlbedoSum / Math.pow( Parameters.getGridSize(), 2 );

            mAreaBlack = scaleArea( mAreaBlack );
            mAreaBlue = scaleArea( mAreaBlue );
            mAreaGreen = scaleArea( mAreaGreen );
            mAreaOrange = scaleArea( mAreaOrange );
            mAreaRed = scaleArea( mAreaRed );

            mSite = mNewSite;

            if( Parameters.isMutationEnabled() ) {
                mPlotter.addTopData( 0, lum, mAreaBlack );
                mPlotter.addTopData( 1, lum, mAreaBlue );
                mPlotter.addTopData( 2, lum, mAreaGreen );
                mPlotter.addTopData( 3, lum, mAreaOrange );
                mPlotter.addTopData( 4, lum, mAreaRed );
            } else {
                mPlotter.addTopData( 0, lum, mAreaBlue );
                mPlotter.addTopData( 1, lum, mAreaOrange );
            }
            mPlotter.addBottomData( lum, mTempGlobal );

            resetVariables();
        }

        mPanel = mPlotter.plotGraph();
    }

    /**
     * Getter for property panel.
     *
     * @return Value of property panel.
     */
    public ChartPanel getPanel() {
        return mPanel;
    }
}
